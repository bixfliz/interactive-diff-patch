# interactive-diff-patch

Compares two folders and allow you  to select files that are different and view and merge each difference selectively from the command line.

Requires diff and patch on Linux (usually comes standard on Linux) and Git and robocopy (comes with windows) on windows.  Diff and patch are ususally included in most Linux distros.  Robocopy is included with Windows but you will need to install Git on Windows.

This will go download the command line editor Nano (https://www.nano-editor.org/, GPL) for editing the config file on Windows.

<pre>
Bixfliz idiff-patch (interactive diff and merge/patch)

  Bixfliz interactive folder compare and merge tool.  
  Start with no command line options to run in 
  interactive mode.  Projects and settings can be set 
  in the config located file at 
  /home/username/.interactive-diff-patch/config.toml.   
                                                                                
  Example:                                                                      
  idiff-patch -l /var/test-code -r /var/code -f node_modules;.git -q y -t 100   

Options

  -l, --LeftFolder string    Left Folder - the one with 
                             the changes.                                       
  -r, --RightFolder string   Right Folder.                                                                 
  -x, --XFiles string        File patters to ignore.  
                             Example: *.txt;*.png;*.bak                           
  -f, --XFolders string      Folder patters to ignore.  
                             Example: .git;node_modules;test*                   
  -q, --AskQuestions string   Set to 'y' to ask questions.  
                             Default is 'n'.                                
  -t, --Threads number       In Windows you can specify the 
                             number of threads to use for 
                             the folder        
                             difference scan.  The default 
                             is 64.                                          
  -h, --help                 View help.               
  </pre>

Demo:
[![asciicast](https://asciinema.org/a/249690.svg)](https://asciinema.org/a/249690)

# Installing

## Through NPM

`[sudo] npm i -g interactive-diff-patch`

## Installing on Arch Linux

`trizen -S interactive-diff-patch`

## Installing on Ubuntu or other Debian distro

Download from:
https://bixfliz.com/files/interactive-diff-patch_0.0.8-0ubuntu1_all.deb
Then run:
`sudo dpkg -i interactive-diff-patch_0.0.8-0ubuntu1_all.deb`
`sudo apt install -f`

## Install on Linux using Snap

Download from:
https://bixfliz.com/files/idiff-patch_0.0.8_amd64.snap
Then run
`sudo snap install idiff-patch_0.0.8_amd64.snap`

# Todo:

* Change prompt for excludes if tiny-glob type is selected from the config file.
