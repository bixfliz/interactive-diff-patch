#!/usr/bin/env node
import {readChunkSync} from 'read-chunk'
import https from 'https'
import inquirerAutocompletePrompt from 'inquirer-autocomplete-prompt'
import inquirer from 'inquirer'
import * as fs from 'fs'
import path from 'path'
import * as chalk from 'chalk'
import * as toml from '@iarna/toml'
import * as os from 'os'
import spawn from 'cross-spawn'
import pkg_lodash from 'lodash';
const { random: lodashRandom } = pkg_lodash;
import ora, { Ora } from 'ora'
import * as temp from 'temp'
import moment from 'moment'
import dirPrompt from 'inquirer-directory'
import { FilterResult } from 'fuzzy'
import pkg_fuzzy from 'fuzzy';
const { filter: fuzzyFilter } = pkg_fuzzy
import { ChildProcess, exec, execSync } from 'child_process'
import getAppDataPath from 'appdata-path'
import request from 'request'
import Table from 'cli-table3'
import clikey from 'clikey'
import fsExta from 'fs-extra'
import commandLineArgs from 'command-line-args'
import commandLineUsage from 'command-line-usage'
import termSize from 'term-size'
import glob from 'tiny-glob'
import * as jsdiff from 'diff'

interface ConfigInfoProject {
    Name: string
    LeftPath: string
    RightPath: string
    FilePattern: string
    ExcludeFilePattern: string
    ExcludeFolderPattern: string
    AskQuestions: string
}
interface ConfigInfo {
    DiffMethod: string
    TextEditorCommand: string
    Project: ConfigInfoProject[]
}

class DiffPatch {
    private oConfigInfo: ConfigInfo
    private oDefaultConfigInfo: ConfigInfo = {
        DiffMethod: 'normal',
        TextEditorCommand: 'default',
        Project: [
            {
                Name: 'Code Compare',
                LeftPath: '/home/code1',
                RightPath: '/home/jason/code2',
                FilePattern: '**/*!({pdf,doc,node_modules})*',
                ExcludeFilePattern: '',
                ExcludeFolderPattern: '',
                AskQuestions: 'false',
            },
            {
                Name: 'Text Compare',
                LeftPath: '/home/text1',
                RightPath: '/home/jason/text2',
                FilePattern: '',
                ExcludeFilePattern: '*.jpg;*.png;*.pdf',
                ExcludeFolderPattern: 'node_modules',
                AskQuestions: 'false',
            },
        ],
    }
    private strTomlConfigFolder: string = os.homedir() + '/.interactive-diff-patch/'
    private strTomlConfigFile: string = this.strTomlConfigFolder + 'config.toml'
    private oConfigProject: ConfigInfoProject

    private strLeftPath: string = ''
    private strRightPath: string = ''
    private strFileExcludes: string = ''
    private strFolderExcludes: string = ''

    private oTimerStart: moment.Moment

    private strArrListOfFileChanges: string[] = []
    private strNanoGitWinPath = 'c:\\Program Files\\Git\\usr\\bin\\nano.exe'
    private strDiffGitWinPath = 'c:\\Program Files\\Git\\usr\\bin\\diff.exe'
    private strPatchGitWinPath = 'c:\\Program Files\\Git\\usr\\bin\\patch.exe'

    private options: commandLineArgs.CommandLineOptions

    private terminalSize = termSize()

    public constructor() {
        const optionDefinitions = [
            {
                name: 'LeftFolder',
                alias: 'l',
                type: String,
                description: 'Left Folder - the one with the changes.',
            },
            {
                name: 'RightFolder',
                alias: 'r',
                type: String,
                description: 'Right Folder.',
            },
            {
                name: 'XFiles',
                alias: 'x',
                type: String,
                description: 'File patters to ignore.  Example: *.txt;*.png;*.bak',
            },
            {
                name: 'XFolders',
                alias: 'f',
                type: String,
                description: 'Folder patters to ignore.  Example: .git;node_modules;test*',
            },
            {
                name: 'AskQuestions',
                alias: 'q',
                type: String,
                description: "Set to 'n' to skip questions.  Default is 'n'.",
            },
            { name: 'help', alias: 'h', description: 'View help.' },
        ]
        const sections = [
            {
                header: 'Bixfliz idiff-patch (interactive diff and merge/patch)',
                content:
                    'Bixfliz interactive folder compare and merge tool.  Start with no command line options to run in interactive mode.  Projects and settings can be set in the config located file at ' +
                    this.strTomlConfigFile +
                    '.  The DiffMethod option in the config file can be "normal", "diff", or "robocopy" (robocopy is only for Windows).  \n\nExample:\nidiff-patch -l /var/test-code -r /var/code -f node_modules;.git -q y',
            },
            {
                header: 'Options',
                optionList: optionDefinitions,
            },
        ]

        this.options = commandLineArgs(optionDefinitions)

        if (this.options.help === null) {
            console.log(commandLineUsage(sections))
            process.exit()
        }

        if (!fs.existsSync(this.strTomlConfigFolder + '/')) {
            fs.mkdirSync(this.strTomlConfigFolder + '/')
        }
        if (!fs.existsSync(this.strTomlConfigFile)) {
            fs.closeSync(fs.openSync(this.strTomlConfigFile, 'w'))
            let defaultConfigData: {} = {
                Project: this.oDefaultConfigInfo.Project,
            }
            fs.writeFileSync(this.strTomlConfigFile, toml.stringify(defaultConfigData), { flag: 'w+' })
        }
        if (os.type() === 'Windows_NT') {
            this.getNano().then(
                (): void => {
                    this.selectCompareFolders()
                },
            )
        } else {
            this.selectCompareFolders()
        }
    }

    private uuidv4(): string {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(
            /[xy]/g,
            (c): string => {
                let r = (Math.random() * 16) | 0,
                    v = c == 'x' ? r : (r & 0x3) | 0x8
                return v.toString(16)
            },
        )
    }

    private backupFile(Fullpath: string): void {
        let strAppDataFolder: string = getAppDataPath('interactive-diff-patch')
        let strAppDataBackupFolder: string = path.join(strAppDataFolder, 'backups')
        if (!fs.existsSync(strAppDataFolder)) {
            fs.mkdirSync(strAppDataFolder)
        }
        if (!fs.existsSync(strAppDataBackupFolder)) {
            fs.mkdirSync(strAppDataBackupFolder)
        }
        let strFilename: string = path.basename(Fullpath)
        let strBackupPath: string = path.join(strAppDataBackupFolder, this.uuidv4() + strFilename)
        fs.copyFileSync(Fullpath, strBackupPath)
        console.log('File backed up to: ' + strBackupPath)
    }

    private async getNano(): Promise<void> {
        try {
            return new Promise(
                (resolve, reject): void => {
                    setTimeout(
                        (): void => {
                            try {
                                if (fs.existsSync(this.strNanoGitWinPath)) {
                                    resolve()
                                }
                                let strAppDataFolder: string = getAppDataPath('interactive-diff-patch')
                                let strDownloadFilePath: string = strAppDataFolder + '/nano.exe'
                                this.strTomlConfigFile = this.strTomlConfigFile.replace(/\\/g, '/')
                                let booDownloadExists: boolean = fs.existsSync(strDownloadFilePath)
                                if (booDownloadExists) {
                                    resolve()
                                } else {
                                    if (!fs.existsSync(strAppDataFolder)) {
                                        fs.mkdirSync(strAppDataFolder)
                                    }
                                    let strBaseUrl = 'https://www.nano-editor.org/dist/win32-support/'
                                    this.getWebPage(strBaseUrl).then(
                                        (strHtml: string): void => {
                                            let oMatch = /<a href="(nano-git-[0-9A-z]+\.exe)">/gi.exec(strHtml)
                                            let strNanoWinUrl = ''
                                            if (!fs.existsSync(strAppDataFolder)) {
                                                fs.mkdirSync(strAppDataFolder)
                                            }
                                            if (oMatch && oMatch[1]) {
                                                strNanoWinUrl = oMatch[1]
                                            }
                                            if (strNanoWinUrl) {
                                                this.download(
                                                    strBaseUrl + strNanoWinUrl,
                                                    strDownloadFilePath,
                                                    (msg: string): void => {
                                                        if (msg) {
                                                            console.log(msg)
                                                            reject(msg)
                                                        } else {
                                                            resolve()
                                                        }
                                                    },
                                                )
                                            }
                                        },
                                    )
                                }
                            } catch (error) {
                                reject(error)
                            }
                        },
                    )
                },
            )
        } catch (error) {
            console.error(error.message)
            process.exit(-1)
        }
    }

    private async download(url: string, dest: string, cb: (respon: string) => void): Promise<void> {
        try {
            var file = fs.createWriteStream(dest)
            var sendReq = request.get(url)

            // verify response code
            sendReq.on(
                'response',
                (response): void => {
                    if (response.statusCode !== 200) {
                        return cb('Response status was ' + response.statusCode)
                    }
                },
            )

            // check for request errors
            sendReq.on(
                'error',
                (err): void => {
                    fs.unlinkSync(dest)
                    return cb(err.message)
                },
            )

            sendReq.pipe(file)

            file.on(
                'finish',
                (cb): void => {
                    file.close() // close() is async, call cb after close completes.
                    return cb('')
                },
            )

            file.on(
                'error',
                (err): void => {
                    // Handle errors
                    fs.unlinkSync(dest) // Delete the file async. (But we don't check the result)
                    return cb(err.message)
                },
            )
        } catch (error) {
            console.error(error.message)
            process.exit(-1)
        }
    }

    private async selectCompareFolders(): Promise<void> {
        try {
            this.strLeftPath = ''
            this.strRightPath = ''
            this.strFileExcludes = ''
            this.strFolderExcludes = ''
            let strData: string = fs.readFileSync(this.strTomlConfigFile).toString()
            let oData = toml.parse(strData)
            this.oConfigInfo = (oData as unknown) as ConfigInfo

            let oCliTable = new Table({
                style: { head: [], border: [] },
            })

            if (this.options.LeftFolder && this.options.RightFolder) {
                this.strLeftPath = this.options.LeftFolder
                this.strRightPath = this.options.RightFolder
                let strFileExcludes = ''
                let strFolderExcludes = ''
                let strAskQuestions = ''
                if (this.options.XFiles) strFileExcludes = this.options.XFiles
                if (this.options.XFolders) strFolderExcludes = this.options.XFolders
                if (this.options.AskQuestions && this.options.AskQuestions.toLowerCase() == 'y') {
                    strAskQuestions = 'true'
                } else {
                    strAskQuestions = 'false'
                }
                this.oConfigProject = {
                    Name: 'Args',
                    LeftPath: this.strLeftPath,
                    RightPath: this.strRightPath,
                    FilePattern: '',
                    ExcludeFilePattern: strFileExcludes,
                    ExcludeFolderPattern: strFolderExcludes,
                    AskQuestions: strAskQuestions,
                }
            } else {
                oCliTable.push(
                    [
                        {
                            colSpan: 2,
                            content: chalk.default.cyanBright('Press a Key to Select Action:'),
                        },
                    ],
                    [chalk.default.greenBright('p'), 'Start by selecting a project'],
                    [chalk.default.greenBright('m'), 'Enter Values Manually'],
                    [chalk.default.greenBright('e'), 'Edit system config file'],
                )

                console.log('\n\n')
                console.log(oCliTable.toString())

                let strLetter: string = await clikey('?')
                console.log('>' + strLetter)

                console.log('\n\n')
                if (strLetter.startsWith('e')) {
                    this.editFileSync(this.strTomlConfigFile)
                    this.selectCompareFolders()
                    return
                } else if (strLetter.startsWith('p')) {
                    let strArrProject: string[] = []
                    for (let oProject of this.oConfigInfo.Project) {
                        strArrProject.push(oProject.Name)
                    }

                    console.log('\n\n')
                    let strProject: string = await this.askForListChoiceFuzzy('Project:', strArrProject)

                    for (var intEachItem = 0; intEachItem < this.oConfigInfo.Project.length; intEachItem++) {
                        let oProject = this.oConfigInfo.Project[intEachItem]

                        if (oProject.Name == strProject) {
                            this.oConfigProject = oProject
                            this.strFileExcludes = this.oConfigProject.ExcludeFilePattern
                            this.strFolderExcludes = this.oConfigProject.ExcludeFolderPattern
                            this.strLeftPath = this.oConfigProject.LeftPath
                            this.strRightPath = this.oConfigProject.RightPath
                            if (
                                !this.oConfigProject ||
                                (this.oConfigProject.AskQuestions && this.oConfigProject.AskQuestions === 'true')
                            ) {
                                console.log('\n\n')
                                this.strFileExcludes = await this.askForString(
                                    'Exclude file pattern: ',
                                    this.strFileExcludes,
                                )
                                console.log('\n\n')
                                this.strFolderExcludes = await this.askForString(
                                    'Exclude folder pattern: ',
                                    this.strFolderExcludes,
                                )
                            }

                            // this.selectFile()
                            break
                        }
                    }
                } else if (strLetter.startsWith('m')) {
                    console.log('\n\n')
                    this.strFileExcludes = await this.askForString(
                        'Exclude file patteren (e.g.: *.png;*.jpg;*.pdf;): ',
                        '',
                    )
                    console.log('\n\n')
                    this.strFolderExcludes = await this.askForString(
                        'Exclude file patteren (e.g.: node_modules;*-test): ',
                        '',
                    )
                }
            }

            let booDiffTwoFiles = false
            let booLIsFile = false
            let booRIsFile = false

            if (this.strLeftPath) {
                if (!fs.existsSync(this.strLeftPath)) {
                    console.error(this.strLeftPath + ' not found.')
                    process.exit(-1)
                }
                let LStats = fs.statSync(this.strLeftPath)
                booLIsFile = LStats.isFile()
            } else {
                this.strLeftPath = '/'
            }
            if (this.strRightPath) {
                if (!fs.existsSync(this.strRightPath)) {
                    console.error(this.strRightPath + ' not found.')
                    process.exit(-1)
                }
                let RStats = fs.statSync(this.strRightPath)
                booRIsFile = RStats.isFile()
            } else {
                this.strRightPath = '/'
            }
            if (booLIsFile && booRIsFile) {
                booDiffTwoFiles = true
            }

            let strFolderSep = '/'
            if (os.type() === 'Windows_NT') {
                strFolderSep = '\\'
                this.strLeftPath = this.strLeftPath.replace(/\//g, '\\')
                this.strLeftPath = this.strLeftPath.replace(/\\\\/g, '\\')

                this.strRightPath = this.strRightPath.replace(/\//g, '\\')
                this.strRightPath = this.strRightPath.replace(/\\\\/g, '\\')
            } else {
                this.strLeftPath = this.strLeftPath.replace(/\\/g, '/')
                this.strLeftPath = this.strLeftPath.replace(/\/\//g, '/')

                this.strRightPath = this.strRightPath.replace(/\\/g, '/')
                this.strRightPath = this.strRightPath.replace(/\/\//g, '/')
            }

            if (!booDiffTwoFiles) {
                if (!this.strLeftPath.endsWith(strFolderSep)) this.strLeftPath = this.strLeftPath + strFolderSep
                if (!this.strRightPath.endsWith(strFolderSep)) this.strRightPath = this.strRightPath + strFolderSep
            }

            if (
                (this.oConfigProject &&
                    this.oConfigProject.AskQuestions &&
                    this.oConfigProject.AskQuestions === 'true') ||
                (!this.oConfigProject && booDiffTwoFiles === false)
            ) {
                /*                 if (os.type() === 'Windows_NT') {
                    if (this.strLeftPath == '/') {
                        this.strLeftPath = os.homedir() // + '\\'
                    } else if (this.strLeftPath == '\\') {
                        this.strLeftPath = os.homedir() // + '\\'
                    }
                    if (this.strRightPath == '/') {
                        this.strRightPath = os.homedir() // + '\\'
                    } else if (this.strRightPath == '\\') {
                        this.strRightPath = os.homedir() // K+ '\\'
                    }
                } */

                let strSelectedSubPathLeft: string
                let strSelectedSubPathRight: string

                console.log('\n\n')
                strSelectedSubPathLeft = await this.askForFolder('Select first folder:', this.strLeftPath)
                console.log('\n\n')
                strSelectedSubPathRight = await this.askForFolder('Select second folder:', this.strRightPath)
                this.strLeftPath = path.join(this.strLeftPath, strSelectedSubPathLeft)
                this.strRightPath = path.join(this.strRightPath, strSelectedSubPathRight)
                this.loadFolderDifferences()
            } else if (booDiffTwoFiles) {
                this.showColorDiff(this.strLeftPath, this.strRightPath, 1, true)
            } else {
                this.loadFolderDifferences()
            }
        } catch (error) {
            throw error
        }
    }

    private async loadFolderDifferences(): Promise<void> {
        try {
            if (this.oConfigInfo.DiffMethod && this.oConfigInfo.DiffMethod.toLowerCase() == 'robocopy') {
                await this.loadFolderDifferencesWithDiff(true)
            } else if (this.oConfigInfo.DiffMethod && this.oConfigInfo.DiffMethod.toLowerCase() == 'diff') {
                await this.loadFolderDifferencesWithDiff()
            } else {
                await this.loadFolderDifferencesWithTinyGlob()
            }
            return
        } catch (error) {
            throw error
        }
    }

    private async loadFolderDifferencesWithTinyGlob(): Promise<void> {
        try {
            const spinner = ora({
                text: 'Loading differences...',
                color: 'yellow',
                spinner: 'circle',
            })
            spinner.start()
            let strExcludes = ''
            if (this.oConfigProject.FilePattern) {
                strExcludes = this.oConfigProject.FilePattern
            } else {
                strExcludes = '**/*'
            }

            let strFolderPath: string = this.strLeftPath.replace(/\\/g, '/')
            if (!fs.existsSync(this.strLeftPath)) {
                console.error(this.strLeftPath + ' not found.')
                process.exit(-1)
            }
            if (!fs.existsSync(this.strRightPath)) {
                console.error(this.strRightPath + ' not found.')
                process.exit(-1)
            }

            setTimeout((): void => {
                glob(strExcludes, {
                    cwd: strFolderPath,
                    filesOnly: true,
                }).then(
                    (strFiles: string[]): void => {
                        let oTimerStart: moment.Moment = moment()
                        this.strArrListOfFileChanges = []
                        for (let i = 0; i < strFiles.length; i++) {
                            let strFileRelativePath = strFiles[i]
                            let fullPath = path.join(this.strLeftPath, strFileRelativePath)
                            let strFileTwoFullPath = this.strRightPath + strFileRelativePath
                            let stats1 = fs.statSync(fullPath) //, (err, stats1) => {
                            if (fs.existsSync(strFileTwoFullPath)) {
                                let stats2 = fs.statSync(strFileTwoFullPath) //, (err, stats2) => {
                                let nTimeDiffInSeconds = Math.abs(
                                    (stats1.mtime.getTime() - stats2.mtime.getTime()) / 1000,
                                )

                                if (stats1.size > stats2.size) {
                                    this.strArrListOfFileChanges.push('   Bigger: ' + strFileRelativePath)
                                } else if (stats1.size < stats2.size) {
                                    this.strArrListOfFileChanges.push('  Smaller: ' + strFileRelativePath)
                                } else if (nTimeDiffInSeconds > 1 && stats1.mtime > stats2.mtime) {
                                    let i = 0
                                    let oData1: Buffer
                                    let oData2: Buffer
                                    do {
                                        oData1 = readChunkSync(fullPath, {length: i * 10000, startPosition: 10000});
                                        oData2 = readChunkSync(strFileTwoFullPath, {length: i * 10000, startPosition: 10000});
                                        if (oData1.compare(oData2) != 0) {
                                            this.strArrListOfFileChanges.push('    Newer: ' + strFileRelativePath)
                                            break
                                        }
                                        i++
                                    } while (
                                        oData1.byteLength &&
                                        oData2.byteLength &&
                                        oData1.byteLength == 10000 &&
                                        oData2.byteLength == 10000
                                    )
                                } else if (nTimeDiffInSeconds > 1 && stats1.mtime < stats2.mtime) {
                                    let i = 0
                                    let oData1: Buffer
                                    let oData2: Buffer
                                    do {
                                        oData1 = readChunkSync(fullPath, {length: i * 10000, startPosition: 10000});
                                        oData2 = readChunkSync(strFileTwoFullPath, {length: i * 10000, startPosition: 10000});
                                        if (oData1.compare(oData2) != 0) {
                                            this.strArrListOfFileChanges.push('    Older: ' + strFileRelativePath)
                                            break
                                        }
                                        i++
                                    } while (
                                        oData1.byteLength &&
                                        oData2.byteLength &&
                                        oData1.byteLength == 10000 &&
                                        oData2.byteLength == 10000
                                    )
                                }
                            } else {
                                this.strArrListOfFileChanges.push('< <  Only: ' + strFileRelativePath)
                            }
                        }
                        if (
                            this.strArrListOfFileChanges &&
                            this.strArrListOfFileChanges.length &&
                            this.strArrListOfFileChanges[this.strArrListOfFileChanges.length - 1].trim().length == 0
                        )
                            this.strArrListOfFileChanges.pop()
                        this.showFolderDifferences(oTimerStart, spinner)
                    },
                )
            }, 1000)
        } catch (error) {
            console.error(error.message)
            process.exit(-1)
        }
    }

    private async loadFolderDifferencesWithDiff(UseRobocopy?: boolean): Promise<void> {
        try {
            let strCommand = ''

            let strArrFolderDiffSwitches: string[] = ['--brief', '--recursive']
            let strFolderDiffTempFilename: string = temp.path({ suffix: '.diff' })
            let strDiffProgramExcludeFilename: string = temp.path({
                suffix: '-excludes.txt',
            })

            if (UseRobocopy) {
                //
                strCommand = 'robocopy'
                strArrFolderDiffSwitches = [
                    this.strLeftPath,
                    this.strRightPath,
                    '/log:' + strFolderDiffTempFilename,
                    '/L',
                    '/e',
                    '/ns',
                    '/njs',
                    '/njh',
                    '/ndl',
                    '/fp',
                    '/np',
                    '/fft',
                ]
                if (this.strFileExcludes) {
                    this.strFileExcludes = this.strFileExcludes.replace(/\;/g, ' ')
                    this.strFileExcludes = this.strFileExcludes.replace(/\[\)\]/g, ')')
                    this.strFileExcludes = this.strFileExcludes.replace(/\[\(\]/g, '(')
                    strArrFolderDiffSwitches.push('/XF')
                    strArrFolderDiffSwitches = strArrFolderDiffSwitches.concat(this.strFileExcludes.split(' '))
                }
                if (this.strFolderExcludes) {
                    this.strFolderExcludes = this.strFolderExcludes.replace(/\;/g, ' ')
                    this.strFolderExcludes = this.strFolderExcludes.replace(/\[\)\]/g, ')')
                    this.strFolderExcludes = this.strFolderExcludes.replace(/\[\(\]/g, '(')
                    strArrFolderDiffSwitches.push('/XD')
                    strArrFolderDiffSwitches = strArrFolderDiffSwitches.concat(this.strFolderExcludes.split(' '))
                }
            } else {
                strCommand = 'diff'
                let strExcludeListForDiffProg = ''
                if (this.strFileExcludes && this.strFolderExcludes) {
                    strExcludeListForDiffProg = this.strFileExcludes + ';' + this.strFolderExcludes
                    strExcludeListForDiffProg = strExcludeListForDiffProg.replace(/\;/g, '\n')
                    strExcludeListForDiffProg = strExcludeListForDiffProg.replace(/\n\n/g, '\n')
                }

                fs.writeFileSync(strDiffProgramExcludeFilename, strExcludeListForDiffProg, { flag: 'w+' })
                strArrFolderDiffSwitches.push('-X ' + strDiffProgramExcludeFilename)
                strArrFolderDiffSwitches.push(this.strLeftPath)
                strArrFolderDiffSwitches.push(this.strRightPath)
                strArrFolderDiffSwitches.push('>')
                strArrFolderDiffSwitches.push(strFolderDiffTempFilename)
            }

            console.log(
                chalk.default.grey.dim.italic('\n\nRunning: ' + strCommand + ' ' + strArrFolderDiffSwitches.join(' ')),
            )
            console.log('\n\n')

            const spinner = ora({
                text: 'Loading differences...',
                color: 'yellow',
                spinner: 'circle',
            })
            spinner.start()
            let oTimerStart: moment.Moment = moment()
            if (UseRobocopy) {
                //
                const oChildProcess: ChildProcess = spawn(strCommand, strArrFolderDiffSwitches)

                oChildProcess.stdout.on(
                    'error',
                    (chunk: string): void => {
                        console.log(chunk.toString())
                    },
                )
                oChildProcess.stdout.on(
                    'data',
                    (_chunk: string): void => {
                        // console.log(chunk.toString())
                    },
                )
                oChildProcess.on(
                    'exit',
                    async (): Promise<void> => {
                        let strFolderDiffOutput: string = fs.readFileSync(strFolderDiffTempFilename).toString()
                        let strLeftPathForRegex: string = this.strLeftPath.replace(/\\/g, '\\\\')
                        let strRightPathForRegex: string = this.strRightPath.replace(/\\/g, '\\\\')

                        let oNewReplaceRegex = new RegExp('^New File +' + strLeftPathForRegex + '(.*?)$', 'gm')
                        let oExtraReplaceRegex = new RegExp('^\\*EXTRA File +' + strRightPathForRegex + '(.*?)$', 'gm')
                        let oOlderReplaceRegex = new RegExp('^Older +' + strLeftPathForRegex + '(.*?)$', 'gm')
                        let oNewerReplaceRegex = new RegExp('^Newer +' + strLeftPathForRegex + '(.*?)$', 'gm')
                        let oChangedReplaceRegex = new RegExp('^Changed +' + strLeftPathForRegex + '(.*?)$', 'gm')

                        strFolderDiffOutput = strFolderDiffOutput.replace(/\t/g, '')
                        strFolderDiffOutput = strFolderDiffOutput.replace(/\r/g, '')
                        strFolderDiffOutput = strFolderDiffOutput.replace(/^ */gm, '')
                        strFolderDiffOutput = strFolderDiffOutput.replace(/^$\n/gm, '')
                        // strFolderDiffOutput = strFolderDiffOutput.replace(oOlderReplaceRegex, 'Different: $1')
                        strFolderDiffOutput = strFolderDiffOutput.replace(oOlderReplaceRegex, '< < Older: $1')
                        // strFolderDiffOutput = strFolderDiffOutput.replace(oNewerReplaceRegex, 'Different: $1')
                        strFolderDiffOutput = strFolderDiffOutput.replace(oNewerReplaceRegex, ' > >Newer: $1')
                        strFolderDiffOutput = strFolderDiffOutput.replace(oChangedReplaceRegex, 'Different: $1')
                        strFolderDiffOutput = strFolderDiffOutput.replace(oNewReplaceRegex, '< <  Only: $1')
                        strFolderDiffOutput = strFolderDiffOutput.replace(oExtraReplaceRegex, ' > > Only: $1')
                        strFolderDiffOutput = strFolderDiffOutput.replace(/< <  Only: \//gm, '< <  Only: ')
                        strFolderDiffOutput = strFolderDiffOutput.replace(/ > > Only: \//gm, ' > > Only: ')

                        if (strFolderDiffOutput.trim()) {
                            let strArrChoices: string[] = strFolderDiffOutput.split('\n')

                            if (strArrChoices && strArrChoices[0].trim().length == 0) strArrChoices.shift()
                            if (strArrChoices && strArrChoices[strArrChoices.length - 1].trim().length == 0)
                                strArrChoices.pop()
                        }

                        fs.unlink(
                            strFolderDiffTempFilename,
                            (err): void => {
                                if (err) console.log(err.message)
                            },
                        )
                        this.showFolderDifferences(oTimerStart, spinner)
                    },
                )
            } else {
                var child = exec(strCommand + ' ' + strArrFolderDiffSwitches.join(' '))
                child.stdout.on(
                    'data',
                    (_data): void => {
                        // console.log('stdout: ' + data)
                    },
                )
                child.stderr.on(
                    'error',
                    (data): void => {
                        console.log('eror: ' + data)
                    },
                )
                child.on(
                    'close',
                    (_code): void => {
                        let strFolderDiffOutput: string = fs.readFileSync(strFolderDiffTempFilename).toString()
                        if (os.type() == 'Windows_NT') {
                            strFolderDiffOutput = strFolderDiffOutput.replace(/\//g, '\\')
                        }
                        let strLeftPathForRegex: string = this.strLeftPath.replace(/\\/g, '\\\\')
                        let strRightPathForRegex: string = this.strRightPath.replace(/\\/g, '\\\\')

                        // Different:
                        // < <  Only:
                        //  > > Only:

                        let oDifferReplaceRegex = new RegExp(
                            '^Files ' + strLeftPathForRegex + '(.+) and ' + strRightPathForRegex + '.+ differ$',
                            'gm',
                        )
                        let oLeftOnlyReplaceRegex = new RegExp(
                            '^Only in ' + strLeftPathForRegex + '(.*?): (.+?)$',
                            'gm',
                        )
                        let oRightOnlyReplaceRegex = new RegExp(
                            '^Only in ' + strRightPathForRegex + '(.*?): (.+?)$',
                            'gm',
                        )

                        strFolderDiffOutput = strFolderDiffOutput.replace(/\t/g, '')
                        strFolderDiffOutput = strFolderDiffOutput.replace(/\r/g, '')
                        strFolderDiffOutput = strFolderDiffOutput.replace(/^$\n/gm, '')
                        strFolderDiffOutput = strFolderDiffOutput.replace(oDifferReplaceRegex, 'Different: $1')
                        if (os.type() == 'Windows_NT') {
                            strFolderDiffOutput = strFolderDiffOutput.replace(oLeftOnlyReplaceRegex, '< <  Only: $1/$2')
                            strFolderDiffOutput = strFolderDiffOutput.replace(
                                oRightOnlyReplaceRegex,
                                ' > > Only: $1/$2',
                            )
                        } else {
                            strFolderDiffOutput = strFolderDiffOutput.replace(
                                oLeftOnlyReplaceRegex,
                                '< <  Only: $1\\$2',
                            )
                            strFolderDiffOutput = strFolderDiffOutput.replace(
                                oRightOnlyReplaceRegex,
                                ' > > Only: $1\\$2',
                            )
                        }
                        strFolderDiffOutput = strFolderDiffOutput.replace(/< <  Only: \//gm, '< <  Only: ')
                        strFolderDiffOutput = strFolderDiffOutput.replace(/ > > Only: \//gm, ' > > Only: ')
                        strFolderDiffOutput = strFolderDiffOutput.replace(/< <  Only: \\/gm, '< <  Only: ')
                        strFolderDiffOutput = strFolderDiffOutput.replace(/ > > Only: \\/gm, ' > > Only: ')

                        if (strFolderDiffOutput && strFolderDiffOutput.trim()) {
                            let strArrChoices: string[] = strFolderDiffOutput.split('\n')
                            if (strArrChoices && strArrChoices[0].trim().length == 0) strArrChoices.shift()
                            if (strArrChoices && strArrChoices[strArrChoices.length - 1].trim().length == 0)
                                strArrChoices.pop()
                            this.strArrListOfFileChanges = strArrChoices
                        }
                        fs.unlink(
                            strDiffProgramExcludeFilename,
                            (err): void => {
                                if (err) console.log(err.message)
                            },
                        )
                        this.showFolderDifferences(oTimerStart, spinner)
                    },
                )
            }
        } catch (error) {
            console.error(error.message)
            process.exit(-1)
        }
    }

    private async showFolderDifferences(oTimerStart?: moment.Moment, spinner?: Ora): Promise<void> {
        try {
            if (oTimerStart) {
                if (this.strArrListOfFileChanges && this.strArrListOfFileChanges.length) {
                    let oTimerEnd: moment.Moment = moment()
                    let intMilliseconds: number = oTimerEnd.diff(oTimerStart, 'milliseconds')
                    let intSeconds: number = Math.round(intMilliseconds / 100) / 10
                    let intMinutes: number = Math.round(intMilliseconds / 100 / 60) / 10
                    let strFileCount: string = this.strArrListOfFileChanges.length.toLocaleString()
                    let strStatusLine = ''
                    if (intSeconds == 1) {
                        strStatusLine = strFileCount + ' files in 1 second.'
                    } else if (intSeconds < 60) {
                        strStatusLine = strFileCount + ' files in ' + intSeconds + ' seconds.'
                    } else if (intSeconds == 60) {
                        strStatusLine = strFileCount + ' files in 1 minute.'
                    } else {
                        strStatusLine = strFileCount + ' files in ' + intMinutes.toLocaleString() + ' minutes.'
                    }
                    spinner.text = strStatusLine
                    spinner.succeed()
                } else {
                    spinner.text = 'No Differences'
                    spinner.fail()
                }
            }

            console.log('\n\n')

            let strSelectedLine = await this.askForListChoiceFuzzy('Select a file:', this.strArrListOfFileChanges)

            let strLeftFile = ''
            let strRightFile = ''
            let strSelectedFile: string = strSelectedLine

            // if (os.type() === 'Windows_NT') {
            if (/^Added: /.test(strSelectedLine) || /^< <  Only: /.test(strSelectedLine)) {
                strSelectedFile = strSelectedFile.replace(/^Added: /, '')
                strSelectedFile = strSelectedFile.replace(/^< <  Only: /, '')
                strLeftFile = path.join(this.strLeftPath, strSelectedFile)
                strRightFile = path.join(this.strRightPath, strSelectedFile)

                let oCliTable = new Table({
                    style: { head: [], border: [] },
                })

                oCliTable.push(
                    [
                        {
                            colSpan: 2,
                            content: chalk.default.cyanBright('Press a Key to Select Action:'),
                        },
                    ],
                    [chalk.default.greenBright('d'), 'Delete ' + strLeftFile],
                    [chalk.default.greenBright('c'), 'Copy file to ' + strRightFile],
                    [chalk.default.greenBright('f'), 'Back to Files'],
                )

                console.log('\n\n')
                console.log(oCliTable.toString())

                let strLetter: string = await clikey('?')
                console.log('>' + strLetter)

                if (strLetter == 'd') {
                    console.log('\n\n')
                    console.log('Are you sure you want to delete ' + strLeftFile + '? [yN]')
                    let strConfirm: string = await clikey('?')

                    if (strConfirm.toLowerCase() == 'y') {
                        this.backupFile(strLeftFile)
                        fsExta.unlinkSync(strLeftFile)
                    }
                    this.loadFolderDifferences()
                } else if (strLetter == 'c') {
                    console.log('\n\n')
                    console.log('Are you sure you want to copy ' + strLeftFile + ' to ' + strRightFile + '? [yN]')
                    let strConfirm: string = await clikey('?')

                    if (strConfirm.toLowerCase() == 'y') {
                        this.backupFile(strRightFile)
                        fsExta.copyFileSync(strLeftFile, strRightFile)
                    }
                    this.loadFolderDifferences()
                } else if (strLetter == 'f') {
                    this.showFolderDifferences()
                }
            } else if (/^Extra: /.test(strSelectedLine) || /^ > > Only: /.test(strSelectedLine)) {
                strSelectedFile = strSelectedFile.replace(/^Extra: /, '')
                strSelectedFile = strSelectedFile.replace(/^ > > Only: /, '')
                strLeftFile = path.join(this.strLeftPath, strSelectedFile)
                strRightFile = path.join(this.strRightPath, strSelectedFile)

                let oCliTable = new Table({
                    style: { head: [], border: [] },
                })
                oCliTable.push(
                    [
                        {
                            colSpan: 2,
                            content: chalk.default.cyanBright('Press a Key to Select Action:'),
                        },
                    ],
                    [chalk.default.greenBright('d'), 'Delete ' + strRightFile],
                    [chalk.default.greenBright('c'), 'Copy file to ' + strLeftFile],
                    [chalk.default.greenBright('f'), 'Back to Files'],
                )
                console.log('\n\n')
                console.log(oCliTable.toString())

                let strLetter: string = await clikey('?')
                console.log('>' + strLetter)
                if (strLetter == 'd') {
                    console.log('\n\n')
                    console.log('Are you sure you want to delete ' + strRightFile + '? [yN]')
                    let strConfirm: string = await clikey('?')

                    if (strConfirm.toLowerCase() == 'y') {
                        this.backupFile(strRightFile)
                        fsExta.unlinkSync(strRightFile)
                    }
                    this.loadFolderDifferences()
                } else if (strLetter == 'c') {
                    console.log('\n\n')
                    console.log('Are you sure you want to copy ' + strRightFile + ' to ' + strLeftFile + '? [yN]')
                    let strConfirm: string = await clikey('?')

                    if (strConfirm.toLowerCase() == 'y') {
                        this.backupFile(strLeftFile)
                        fsExta.copyFileSync(strRightFile, strLeftFile)
                    }
                    this.loadFolderDifferences()
                } else if (strLetter == 'f') {
                    this.showFolderDifferences()
                }
            } else if (
                /^Older: /.test(strSelectedLine) ||
                /^ *Older: /.test(strSelectedLine) ||
                /^ *Newer: /.test(strSelectedLine) ||
                /^ *Bigger: /.test(strSelectedLine) ||
                /^ *Smaller: /.test(strSelectedLine) ||
                /^Newer: /.test(strSelectedLine) ||
                /^Different: /.test(strSelectedLine) ||
                /^ > >Newer: /.test(strSelectedLine) ||
                /^< < Older: /.test(strSelectedLine)
            ) {
                strSelectedFile = strSelectedFile.replace(/^Older: /, '')
                strSelectedFile = strSelectedFile.replace(/^Newer: /, '')
                strSelectedFile = strSelectedFile.replace(/^ *Newer: /, '')
                strSelectedFile = strSelectedFile.replace(/^ *Older: /, '')
                strSelectedFile = strSelectedFile.replace(/^ *Bigger: /, '')
                strSelectedFile = strSelectedFile.replace(/^ *Smaller: /, '')
                strSelectedFile = strSelectedFile.replace(/^Different: /, '')
                strSelectedFile = strSelectedFile.replace(/^ > >Newer: /, '')
                strSelectedFile = strSelectedFile.replace(/^< < Older: /, '')
                strLeftFile = path.join(this.strLeftPath, strSelectedFile)
                strRightFile = path.join(this.strRightPath, strSelectedFile)

                this.showColorDiff(strLeftFile, strRightFile, 1)
            }
        } catch (error) {
            throw error
        }
    }
    private formatBytes(a: number, b: number): string {
        if (0 == a) return '0 Bytes'
        var c = 1024,
            d = b || 2,
            e = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            f = Math.floor(Math.log(a) / Math.log(c))
        return parseFloat((a / Math.pow(c, f)).toFixed(d)) + ' ' + e[f]
    }

    private async showColorDiff(
        strLeftFile: string,
        strRightFile: string,
        Hunk: number,
        booNoBackToFiles: boolean = false,
    ): Promise<void> {
        try {
            if (Hunk < 1) Hunk = 1
            //

            //git diff --word-diff=color ../diff-patch-tests/1 ../diff-patch-tests/2
            let strDiffProgram = 'diff'
            // let strArrDiffSwitches: string[] = [ '--color=always', '--unified' ]
            let strArrDiffSwitches: string[] = ['--unified']
            // let strDiffProgram: string = 'git'
            // let strArrDiffSwitches: string[] = [ 'diff', '--word-diff=color', '--unified' ]

            strArrDiffSwitches.push(strLeftFile)
            strArrDiffSwitches.push(strRightFile)

            const proc = spawn(strDiffProgram, strArrDiffSwitches)
            // const proc = spawn('git', strArrDiffSwitches)

            let output = ''
            proc.stdout.on(
                'data',
                (chunk): void => {
                    output += chunk.toString()
                },
            )
            proc.on(
                'exit',
                async (): Promise<void> => {
                    let strPatchHeaderLine = ''
                    console.log(
                        chalk.default.grey.dim.italic(
                            'Running: ' + strDiffProgram + ' ' + strArrDiffSwitches.join(' '),
                        ),
                    )
                    // terminal escape codes: \u001b[36m
                    // ing.pdf"\r\n\u001b[36m@@ -242,10 +242,10 @@\u001b[0m\n \t\t\t\t\t\t target="_b
                    // let strHunks = output.split(/\n\u001b\[36m\@\@ -\d+\,\d+ \+\d+\,\d+ \@\@/)
                    // let strHunks = output.split(/\u001b\[36m\@\@ -\d*\,\d* \+\d*\,\d* \@\@/)
                    let strHunks = []
                    let strHunkForDisplay = ''
                    if (output) {
                        strHunks = output.split(/^@\@ -/gm)
                    }
                    if (strHunks.length > 0) {
                        strPatchHeaderLine = strHunks[0]
                        strHunks.shift()
                        if (Hunk > strHunks.length) {
                            Hunk = strHunks.length
                        }
                    }
                    if (output) {
                        strHunkForDisplay = strHunks[Hunk - 1]
                    }
                    let intLeftStartLine = 0
                    let intRightStartLine = 0
                    let intLeftNumberOfLines = 0
                    let intRightNumberOfLines = 0
                    const regexLineNumberMatches = /^([0-9]+),([0-9]+) \+([0-9]+),([0-9]+) @@/gm.exec(strHunkForDisplay)
                    if (strHunkForDisplay.trim()) {
                        if (
                            regexLineNumberMatches[1] &&
                            regexLineNumberMatches[2] &&
                            regexLineNumberMatches[3] &&
                            regexLineNumberMatches[4]
                        ) {
                            if (/^[0-9]+$/.test(regexLineNumberMatches[1])) {
                                intLeftStartLine = Number(regexLineNumberMatches[1])
                            }
                            if (/^[0-9]+$/.test(regexLineNumberMatches[2])) {
                                intLeftNumberOfLines = Number(regexLineNumberMatches[2])
                            }
                            if (/^[0-9]+$/.test(regexLineNumberMatches[3])) {
                                intRightStartLine = Number(regexLineNumberMatches[3])
                            }
                            if (/^[0-9]+$/.test(regexLineNumberMatches[4])) {
                                intRightNumberOfLines = Number(regexLineNumberMatches[4])
                            }
                        }
                    }

                    // start red
                    // [31m
                    // end red
                    // [0m
                    // start yellow
                    // [32m
                    // end yellow
                    // [0m
                    strHunkForDisplay = strHunkForDisplay.replace(/^\d*\,\d* \+\d*\,\d* \@\@/, '')
                    let strHunkForDisplayLines: string[]

                    if (strHunkForDisplay) {
                        strHunkForDisplayLines = strHunkForDisplay.split('\n')
                    }

                    let strHunkForDisplayLinesA: string[] = []
                    let strHunkForDisplayLinesB: string[] = []
                    let booYellow = false
                    if (strHunkForDisplayLines && strHunkForDisplayLines.length && strHunkForDisplayLines.length > 0) {
                        for (let strLine of strHunkForDisplayLines) {
                            if (strLine.startsWith('+')) {
                                // yellow
                                strLine = strLine.substr(1)
                                strHunkForDisplayLinesB.push(strLine)
                                booYellow = true
                            } else if (strLine.startsWith('-')) {
                                // red
                                strLine = strLine.substr(1)
                                if (booYellow) {
                                    if (strHunkForDisplayLinesA.length > strHunkForDisplayLinesB.length) {
                                        do {
                                            strHunkForDisplayLinesB.push('')
                                        } while (strHunkForDisplayLinesA.length != strHunkForDisplayLinesB.length)
                                    } else if (strHunkForDisplayLinesA.length < strHunkForDisplayLinesB.length) {
                                        do {
                                            strHunkForDisplayLinesA.push('')
                                        } while (strHunkForDisplayLinesA.length != strHunkForDisplayLinesB.length)
                                    }
                                }
                                strHunkForDisplayLinesA.push(strLine)
                                booYellow = false
                            } else {
                                if (booYellow) {
                                    if (strHunkForDisplayLinesA.length > strHunkForDisplayLinesB.length) {
                                        do {
                                            strHunkForDisplayLinesB.push('')
                                        } while (strHunkForDisplayLinesA.length != strHunkForDisplayLinesB.length)
                                    } else if (strHunkForDisplayLinesA.length < strHunkForDisplayLinesB.length) {
                                        do {
                                            strHunkForDisplayLinesA.push('')
                                        } while (strHunkForDisplayLinesA.length != strHunkForDisplayLinesB.length)
                                    }
                                }
                                strHunkForDisplayLinesA.push(strLine)
                                strHunkForDisplayLinesB.push(strLine)
                                booYellow = false
                            }
                        }
                    }

                    let strLeft = ''
                    let strRight = ''
                    let strDiffLR = ''
                    let strDiffRL = ''

                    let diffL: jsdiff.Change[]
                    let diffR: jsdiff.Change[]
                    for (let i = 0; i < strHunkForDisplayLinesA.length - 1; i++) {
                        strLeft = strHunkForDisplayLinesA[i]
                        strRight = strHunkForDisplayLinesB[i]
                        if (strLeft != strRight) {
                            diffL = jsdiff.diffWordsWithSpace(strLeft, strRight)
                            strDiffLR = ''
                            strDiffRL = ''
                            diffL.forEach(
                                (part: { added: any; removed: any; value: string }): void => {
                                    if (part.added) {
                                    } else if (part.removed) {
                                        strDiffLR = strDiffLR + chalk.default.black.bgGreen(part.value)
                                    } else {
                                        strDiffLR = strDiffLR + part.value
                                    }
                                },
                            )
                            strHunkForDisplayLinesA[i] = strDiffLR

                            diffR = jsdiff.diffWordsWithSpace(strRight, strLeft)
                            diffR.forEach(
                                (part: { added: any; removed: any; value: string }): void => {
                                    if (part.added) {
                                    } else if (part.removed) {
                                        strDiffRL = strDiffRL + chalk.default.black.bgRed(part.value)
                                    } else {
                                        strDiffRL = strDiffRL + part.value
                                    }
                                },
                            )
                            strHunkForDisplayLinesB[i] = strDiffRL
                        }
                    }

                    // for (let strLine of strHunkForDisplayLines) {
                    // 	if (strLine.startsWith('+')) {
                    // 		// yellow
                    // 		strHunkForDisplayLinesB.push('[32m' + strLine + '[0m')
                    // 		booYellow = true
                    // 	} else if (strLine.startsWith('[32m')) {
                    // 		// yellow
                    // 		strHunkForDisplayLinesB.push(strLine + '[0m')
                    // 		booYellow = true
                    // 	} else if (strLine.startsWith('-')) {
                    // 		// red
                    // 		if (booYellow) {
                    // 			if (strHunkForDisplayLinesA.length > strHunkForDisplayLinesB.length) {
                    // 				do {
                    // 					strHunkForDisplayLinesB.push('')
                    // 				} while (strHunkForDisplayLinesA.length != strHunkForDisplayLinesB.length)
                    // 			} else if (strHunkForDisplayLinesA.length < strHunkForDisplayLinesB.length) {
                    // 				do {
                    // 					strHunkForDisplayLinesA.push('')
                    // 				} while (strHunkForDisplayLinesA.length != strHunkForDisplayLinesB.length)
                    // 			}
                    // 		}
                    // 		strHunkForDisplayLinesA.push('[31m' + strLine + '[0m')
                    // 		booYellow = false
                    // 	} else if (strLine.startsWith('[31m')) {
                    // 		// red
                    // 		if (booYellow) {
                    // 			if (strHunkForDisplayLinesA.length > strHunkForDisplayLinesB.length) {
                    // 				do {
                    // 					strHunkForDisplayLinesB.push('')
                    // 				} while (strHunkForDisplayLinesA.length != strHunkForDisplayLinesB.length)
                    // 			} else if (strHunkForDisplayLinesA.length < strHunkForDisplayLinesB.length) {
                    // 				do {
                    // 					strHunkForDisplayLinesA.push('')
                    // 				} while (strHunkForDisplayLinesA.length != strHunkForDisplayLinesB.length)
                    // 			}
                    // 		}
                    // 		strHunkForDisplayLinesA.push(strLine + '[0m')
                    // 		booYellow = false
                    // 	} else {
                    // 		if (booYellow) {
                    // 			if (strHunkForDisplayLinesA.length > strHunkForDisplayLinesB.length) {
                    // 				do {
                    // 					strHunkForDisplayLinesB.push('')
                    // 				} while (strHunkForDisplayLinesA.length != strHunkForDisplayLinesB.length)
                    // 			} else if (strHunkForDisplayLinesA.length < strHunkForDisplayLinesB.length) {
                    // 				do {
                    // 					strHunkForDisplayLinesA.push('')
                    // 				} while (strHunkForDisplayLinesA.length != strHunkForDisplayLinesB.length)
                    // 			}
                    // 		}
                    // 		strHunkForDisplayLinesA.push(strLine)
                    // 		strHunkForDisplayLinesB.push(strLine)
                    // 		booYellow = false
                    // 	}
                    // }
                    let intMaxLength: number
                    if (intLeftNumberOfLines > intRightNumberOfLines) {
                        intMaxLength = intLeftNumberOfLines
                    } else {
                        intMaxLength = intRightNumberOfLines
                    }

                    let intColWidth: number = Math.floor(this.terminalSize.columns / 2 - 5)
                    let strLeftFolderFilePath: string = path.dirname(strLeftFile)
                    let strRightFolderFilePath: string = path.dirname(strRightFile)
                    let strLeftFilename: string = path.basename(strLeftFile)
                    let strRightFilename: string = path.basename(strRightFile)
                    let strLeftModeTime: string
                    let strRightModeTime: string
                    let strLeftSize: string
                    let strRightSize: string
                    if (fsExta.existsSync(strLeftFile)) {
                        strLeftModeTime =
                            fsExta.statSync(strLeftFile).mtime.toLocaleDateString() +
                            ' ' +
                            fsExta.statSync(strLeftFile).mtime.toLocaleTimeString()
                        strLeftSize = this.formatBytes(fsExta.statSync(strLeftFile).size, 3)
                    }
                    if (fsExta.existsSync(strRightFile)) {
                        strRightModeTime =
                            fsExta.statSync(strRightFile).mtime.toLocaleDateString() +
                            ' ' +
                            fsExta.statSync(strRightFile).mtime.toLocaleTimeString()
                        strRightSize = this.formatBytes(fsExta.statSync(strRightFile).size, 3)
                    }
                    let oCliDiffTable = new Table({
                        head: [
                            '',
                            chalk.default.cyanBright(
                                'Folder: ' +
                                    strLeftFolderFilePath +
                                    '\n' +
                                    'File: ' +
                                    strLeftFilename +
                                    '\n' +
                                    'line ' +
                                    intLeftStartLine.toLocaleString() +
                                    ' to ' +
                                    (intLeftStartLine + (intLeftNumberOfLines - 2)).toLocaleString() +
                                    '\nModified: ' +
                                    strLeftModeTime +
                                    '\nSize: ' +
                                    strLeftSize,
                            ),
                            chalk.default.cyanBright(
                                'Folder: ' +
                                    strRightFolderFilePath +
                                    '\n' +
                                    'File: ' +
                                    strRightFilename +
                                    '\n' +
                                    'line ' +
                                    intRightStartLine.toLocaleString() +
                                    ' to ' +
                                    (intRightStartLine + (intRightNumberOfLines - 2)).toLocaleString() +
                                    '\nModified: ' +
                                    strRightModeTime +
                                    '\nSize: ' +
                                    strRightSize,
                            ),
                        ],
                        colWidths: [5, intColWidth, intColWidth],
                        wordWrap: true,
                    })
                    let strA: string
                    let strB: string
                    let intLine: number = intLeftStartLine
                    for (let i = 1; i < intMaxLength; i++) {
                        strA = ''
                        strB = ''
                        if (strHunkForDisplayLinesA[i]) {
                            strA = strHunkForDisplayLinesA[i]
                            strA = strA.replace(/\t/gm, '  ')
                            strA = strA.replace(/\r/gm, '')
                        }
                        if (strHunkForDisplayLinesB[i]) {
                            strB = strHunkForDisplayLinesB[i]
                            strB = strB.replace(/\t/gm, '  ')
                            strB = strB.replace(/\r/gm, '')
                        }
                        oCliDiffTable.push([intLine, strA, strB])
                        intLine++
                    }

                    if (strHunks && strHunks.length > 0) console.log(oCliDiffTable.toString())

                    // console.log(strHunkForDisplay)

                    let oCliTable = new Table({
                        style: { head: [], border: [] },
                    })
                    oCliTable.push([
                        {
                            colSpan: 4,
                            content: chalk.default.cyanBright('Press a Key to Select Action:'),
                        },
                    ])

                    if (strHunks.length == 1) {
                        // This is the only one.
                    } else if (strHunks.length > 0 && Hunk == 1) {
                        // This is not the only one but it is the first one.
                        oCliTable.push([chalk.default.greenBright('n'), 'Next Difference', '', ''])
                    } else if (strHunks.length > 0 && Hunk == strHunks.length) {
                        // Last on in the list
                        oCliTable.push([chalk.default.greenBright('p'), 'Previous Difference', '', ''])
                    } else if (strHunks.length == 0) {
                        // if (os.type() === 'Windows_NT') {
                        // Robocopy checks the date of tiles even it they are not different
                        // This showed up on the list but there are no differences.  So, it must be because of the timestamp.
                        let dtm1MTime: Date = fs.statSync(strLeftFile).mtime
                        let dtm2MTime: Date = fs.statSync(strRightFile).mtime
                        let nTimeDiffInSeconds = Math.abs((dtm1MTime.getTime() - dtm2MTime.getTime()) / 1000)
                        if (nTimeDiffInSeconds > 1)
                            oCliTable.push([
                                chalk.default.greenBright('t'),
                                {
                                    colSpan: 3,
                                    content:
                                        'Make Timestamps Equal\nLeft=' +
                                        dtm1MTime.toLocaleDateString() +
                                        ' ' +
                                        dtm1MTime.toLocaleTimeString() +
                                        '\nRight=' +
                                        dtm2MTime.toLocaleDateString() +
                                        ' ' +
                                        dtm2MTime.toLocaleTimeString() +
                                        '\nSet both to Right',
                                },
                            ])
                        // }
                    } else {
                        oCliTable.push([
                            chalk.default.greenBright('n'),
                            'Next Difference',
                            chalk.default.greenBright('p'),
                            'Previous Difference',
                        ])
                    }
                    if (strHunks.length != 0) {
                        // There were hunks returned
                        oCliTable.push(
                            [
                                chalk.default.greenBright('a'),
                                'Apply Left to Right',
                                chalk.default.greenBright('b'),
                                'Apply Right to Left',
                            ],
                            [
                                chalk.default.greenBright('A'),
                                'Copy File from Left to Right',
                                chalk.default.greenBright('B'),
                                'Copy File from Right to Left',
                            ],
                            [chalk.default.greenBright('e'), 'Edit Left', chalk.default.greenBright('E'), 'Edit Right'],
                        )
                    }

                    if (!booNoBackToFiles)
                        oCliTable.push([
                            chalk.default.greenBright('f'),
                            'Back to Files',
                            chalk.default.greenBright('r'),
                            'Restart',
                        ])

                    // console.log('\u2500'.repeat(40))
                    if (strHunks.length) {
                        console.log('Difference ' + Hunk.toString() + ' of ' + strHunks.length.toString() + '.')
                    } else {
                        console.log('No Difference.')
                    }
                    // console.log('\u2500'.repeat(40))
                    console.log(oCliTable.toString())
                    let strLetter: string = await clikey('?')
                    console.log('>' + strLetter)

                    if (!booNoBackToFiles && strLetter == 'r') {
                        this.selectCompareFolders()
                    } else if (strLetter == 'e') {
                        this.backupFile(strLeftFile)
                        if (intLeftStartLine) {
                            this, this.editFileSync(strLeftFile, intLeftStartLine)
                        } else {
                            this, this.editFileSync(strLeftFile)
                        }
                        this.showColorDiff(strLeftFile, strRightFile, Hunk, booNoBackToFiles)
                    } else if (strLetter == 'E') {
                        this.backupFile(strRightFile)
                        if (intRightStartLine) {
                            this, this.editFileSync(strRightFile, intRightStartLine)
                        } else {
                            this, this.editFileSync(strRightFile)
                        }
                        this.showColorDiff(strLeftFile, strRightFile, Hunk, booNoBackToFiles)
                    } else if (strLetter == 'n') {
                        this.showColorDiff(strLeftFile, strRightFile, Hunk + 1, booNoBackToFiles)
                    } else if (strLetter == 'p') {
                        this.showColorDiff(strLeftFile, strRightFile, Hunk - 1, booNoBackToFiles)
                    } else if (!booNoBackToFiles && strLetter == 'f') {
                        console.log('\n\n')
                        console.log('Rescan folders for changes: y/N')
                        let strLetter: string = await clikey('?')
                        console.log('>' + strLetter)
                        if (strLetter.toLowerCase() == 'y') {
                            this.loadFolderDifferences()
                        } else {
                            this.showFolderDifferences()
                        }
                    } else if (strLetter == 't') {
                        let dtrATime: Date = fs.statSync(strRightFile).atime
                        let dtrMTime: Date = fs.statSync(strRightFile).mtime
                        console.log(
                            'Setting last access time on ' +
                                strLeftFile +
                                ' to ' +
                                dtrATime.toLocaleDateString() +
                                ' ' +
                                dtrATime.toLocaleTimeString() +
                                ' and the modified time to ' +
                                dtrMTime.toLocaleDateString() +
                                ' ' +
                                dtrMTime.toLocaleTimeString(),
                        )
                        fs.utimesSync(strLeftFile, fs.statSync(strRightFile).atime, fs.statSync(strRightFile).mtime)
                        console.log('\n\n')
                        if (!booNoBackToFiles) {
                            console.log('Rescan folders for changes: y/N')
                            let strLetter: string = await clikey('?')
                            console.log('>' + strLetter)
                            if (strLetter.toLowerCase() == 'y') {
                                this.loadFolderDifferences()
                            } else {
                                this.showFolderDifferences()
                            }
                        }
                    } else if (strLetter == 'B') {
                        this.backupFile(strLeftFile)
                        fsExta.copyFileSync(strRightFile, strLeftFile)
                        this.showColorDiff(strLeftFile, strRightFile, Hunk - 2, booNoBackToFiles)
                    } else if (strLetter == 'b') {
                        this.backupFile(strLeftFile)
                        let strPatchString: string = strPatchHeaderLine + '@@ -' + strHunks[Hunk - 1]
                        strPatchString = strPatchString.replace(/\x1b\[[0-9;]*m/g, '')
                        temp.track()

                        temp.open(
                            'interactive-diff-patch---',
                            (err, info): void => {
                                if (!err) {
                                    fs.writeFile(
                                        info.fd,
                                        strPatchString,
                                        (err): void => {
                                            if (err) throw err
                                            fs.close(
                                                info.fd,
                                                (err): void => {
                                                    if (err) throw err

                                                    let strCommand = 'patch'
                                                    let strArrPatchSwitches: string[] = [
                                                        '--strip=0',
                                                        '--directory=/',
                                                        '--input=' + info.path,
                                                        strLeftFile,
                                                    ]
                                                    console.log(
                                                        chalk.default.grey.dim.italic(
                                                            'Running: patch ' + strArrPatchSwitches.join(' '),
                                                        ),
                                                    )
                                                    const proc = spawn(strCommand, strArrPatchSwitches)

                                                    proc.on(
                                                        'exit',
                                                        (): void => {
                                                            temp.cleanupSync()
                                                            fs.utimesSync(
                                                                strLeftFile,
                                                                fs.statSync(strRightFile).atime,
                                                                fs.statSync(strRightFile).mtime,
                                                            )

                                                            setTimeout((): void => {
                                                                this.showColorDiff(
                                                                    strLeftFile,
                                                                    strRightFile,
                                                                    Hunk - 2,
                                                                    booNoBackToFiles,
                                                                )
                                                            }, lodashRandom(30, 500))
                                                        },
                                                    )
                                                },
                                            )
                                        },
                                    )
                                }
                            },
                        )
                    } else if (strLetter == 'A') {
                        this.backupFile(strRightFile)
                        fsExta.copyFileSync(strLeftFile, strRightFile)
                        this.showColorDiff(strLeftFile, strRightFile, Hunk - 2, booNoBackToFiles)
                    } else if (strLetter == 'a') {
                        this.backupFile(strRightFile)
                        let strPatchString: string = strPatchHeaderLine + '@@ -' + strHunks[Hunk - 1]
                        strPatchString = strPatchString.replace(/\x1b\[[0-9;]*m/g, '')
                        temp.track()

                        temp.open(
                            'interactive-diff-patch---',
                            (err, info): void => {
                                if (!err) {
                                    fs.writeFile(
                                        info.fd,
                                        strPatchString,
                                        (err): void => {
                                            if (err) throw err
                                            fs.close(
                                                info.fd,
                                                (err): void => {
                                                    if (err) throw err

                                                    let strArrPatchSwitches: string[] = [
                                                        '--strip=0',
                                                        '--directory=/',
                                                        '--reverse',
                                                        '--input=' + info.path,
                                                        strRightFile,
                                                    ]
                                                    console.log(
                                                        chalk.default.grey.dim.italic(
                                                            'Running: patch ' + strArrPatchSwitches.join(' '),
                                                        ),
                                                    )
                                                    const proc = spawn('patch', strArrPatchSwitches)

                                                    // let output = ''
                                                    // proc.stdout.on('data', (chunk) => {
                                                    // 	output += chunk.toString()
                                                    // })
                                                    proc.on(
                                                        'exit',
                                                        async (): Promise<void> => {
                                                            temp.cleanupSync()
                                                            fs.utimesSync(
                                                                strRightFile,
                                                                fs.statSync(strLeftFile).atime,
                                                                fs.statSync(strLeftFile).mtime,
                                                            )
                                                            setTimeout((): void => {
                                                                this.showColorDiff(
                                                                    strLeftFile,
                                                                    strRightFile,
                                                                    Hunk - 2,
                                                                    booNoBackToFiles,
                                                                )
                                                            }, lodashRandom(30, 500))
                                                            // console.log(output)
                                                        },
                                                    )
                                                },
                                            )
                                        },
                                    )
                                }
                            },
                        )
                        // console.log(strPatchString)
                    }
                },
            )
        } catch (error) {
            throw error
        }
    }

    private async askForString(Question: string, Default: string): Promise<string> {
        try {
            return inquirer
                .prompt([
                    {
                        type: 'input',
                        message: Question,
                        name: 'reply',

                        default: (): string => {
                            return Default
                        },
                        filter: (input: string): string => {
                            return input
                        },
                    },
                ])
                .then(
                    (input: { reply: string }): string => {
                        return input.reply
                    },
                )
        } catch (error) {
            throw error
        }
    }
    /* 
  private async askForListChoice(Message: string, Choices: string[]): Promise<string> {
      try {
          console.log(Message)
          return inquirer
              .prompt([
                  {
                      type: 'list',
                      name: 'val',
                      suggestOnly: false,
                      message: Message,
                      choices: Choices,
                      pageSize: 20,
                  },
              ])
              .then(async (answers: { val: string }) => {
                  return answers.val
              })
      } catch (error) {
          throw error
      }
  } */

    private async askForListChoiceFuzzy(Message: string, Choices: string[]): Promise<string> {
        try {
            console.log(Message)

            inquirer.registerPrompt('autocomplete', inquirerAutocompletePrompt)
            return inquirer
                .prompt([
                    {
                        type: 'autocomplete',
                        name: 'val',
                        //suggestOnly: false,
                        message: Message,
                        source: (_answers: string, strUserInput: string): Promise<{}> => {
                            strUserInput = strUserInput || ''
                            return new Promise(
                                (resolve, _reject): void => {
                                    setTimeout((): void => {
                                        let fuzzyResult: FilterResult<string>[] = fuzzyFilter(strUserInput, Choices)

                                        resolve(fuzzyResult.map((el: FilterResult<string>): string => el.original))
                                    }, lodashRandom(30, 500))
                                },
                            )
                        },
                        pageSize: 20,
                    },
                ])
                .then(
                    async (answers: { val: string }): Promise<string> => {
                        return answers.val
                    },
                )
        } catch (error) {
            throw error
        }
    }

    private async askForListChoiceFuzzyForFiles(Message: string, Choices: string[]): Promise<string> {
        try {
            inquirer.registerPrompt('autocomplete', inquirerAutocompletePrompt)
            return inquirer
                .prompt([
                    {
                        type: 'autocomplete',
                        name: 'val',
                        suggestOnly: false,
                        message: Message,
                        pageSize: 20,
                        onKeypress: '',
                        source: (_answers: string, strUserInput: string): Promise<string[]> => {
                            strUserInput = strUserInput || ''
                            return new Promise(
                                (resolve, _reject): void => {
                                    setTimeout((): void => {
                                        let fuzzyResult: FilterResult<string>[] = fuzzyFilter(strUserInput, Choices)

                                        resolve(
                                            fuzzyResult.map(
                                                (el: FilterResult<string>): string => {
                                                    return el.original
                                                },
                                            ),
                                        )
                                    }, lodashRandom(30, 500))
                                },
                            )
                        },
                    },
                ])
                .then(
                    async (answers: { val: string }): Promise<string> => {
                        return answers.val
                    },
                )
        } catch (error) {
            throw error
        }
    }

    private async askForFolder(Message: string, BaseFolderPath: string): Promise<string> {
        try {
            inquirer.registerPrompt('directory', dirPrompt)
            let strPath: string = BaseFolderPath
            return inquirer
                .prompt([
                    {
                        type: 'directory',
                        name: 'path',
                        message: Message,
                        basePath: strPath,
                        options: {
                            displayFiles: false,
                            displayHidden: true,
                        },
                    },
                ])
                .then(
                    (answer: { path: string }): string => {
                        return answer.path
                    },
                )
        } catch (error) {
            throw error
        }
    }

    private getWebPage(url: string): Promise<string> {
        return new Promise(
            (resolve, _reject): void => {
                https
                    .get(
                        url,
                        (resp): void => {
                            let data = ''

                            resp.on(
                                'data',
                                (chunk: string): void => {
                                    data += chunk
                                },
                            )

                            resp.on(
                                'end',
                                (): void => {
                                    resolve(data)
                                },
                            )
                        },
                    )
                    .on(
                        'error',
                        (err): void => {
                            console.log('Error: ' + err.message)
                        },
                    )
            },
        )
    }

    private editFileSync(Filename: string, LineNumber?: number): void {
        let strCommand = 'nano'
        if (!this.oConfigInfo.TextEditorCommand || this.oConfigInfo.TextEditorCommand === 'default') {
            strCommand = 'nano'
        } else {
            strCommand = this.oConfigInfo.TextEditorCommand
        }

        if (os.type() === 'Windows_NT' && strCommand === 'nano') {
            try {
                if (fs.existsSync(this.strNanoGitWinPath)) {
                    strCommand = '"' + this.strNanoGitWinPath + '"'
                } else {
                    let strAppDataFolder: string = getAppDataPath('interactive-diff-patch')
                    let strDownloadFilePath: string = strAppDataFolder + '/nano.exe'
                    strCommand = '"' + strDownloadFilePath + '"'
                    if (!fs.existsSync(strDownloadFilePath)) {
                        strCommand = 'notepad'
                    }
                }
            } catch (error) {
                strCommand = 'notepad'
            }
        }

        if (LineNumber && strCommand === 'nano') {
            strCommand = strCommand + ' -l '

            execSync(strCommand + ' +' + LineNumber.toString() + ' ' + Filename, {
                stdio: [0, 1, 2],
            })
        } else {
            execSync(strCommand + ' ' + Filename, { stdio: [0, 1, 2] })
        }
    }

    //private async compareFoldersSync(_Folder: string): Promise<void> {}

    // compareFoldersSyncOrig(Folder: string) {
    // 	// return new Promise((resolve, reject) => {
    // 	// setTimeout(() => {
    // 	try {
    // 		let files: string[] = fs.readdirSync(Folder)
    // 		// fs.readdir(Folder, (err, files) => {
    // 		for (let i = 0; i < files.length; i++) {
    // 			let file = files[i]
    // 			let fullPath = path.join(Folder, file);
    // 			try {
    // 				let strFileRelativePath = fullPath.substr(this.strLeftPath.length)
    // 				let strFileTwoFullPath = this.strRightPath + strFileRelativePath
    // 				let stats1 = fs.statSync(fullPath)//, (err, stats1) => {
    // 				if (stats1.isDirectory()) {
    // 					this.compareFoldersSync(fullPath)
    // 				} else {
    // 					if (fs.existsSync(strFileTwoFullPath)) {
    // 						let stats2 = fs.statSync(strFileTwoFullPath)//, (err, stats2) => {
    // 						if (stats1.size != stats2.size) {
    // 							this.strArrFileList.push('Sizes Different: ' + fullPath)
    // 						} else if (stats1.mtime > stats2.mtime || stats1.mtime < stats2.mtime) {
    // 							this.strArrFileList.push('Dates Different: ' + fullPath)
    // 						}
    // 						// })
    // 					} else {
    // 						this.strArrFileList.push('Added File: ' + fullPath)
    // 					}
    // 				}
    // 				// })
    // 			} catch (error) {
    // 				// reject(error)
    // 			}
    // 		}
    // 		// })
    // 	} catch (error) {
    // 		// reject(error)
    // 	}
    // 	// })
    // 	// })
    // }

    // async setFileChoices(dir: string) {
    // 	fs.readdir(dir, (err, files) => {
    // 		for (let i = 0; i < files.length; i++) {
    // 			let file = files[i]
    // 			let fullPath = path.join(dir, file);
    // 			try {
    // 				let strFileRelativePath = fullPath.substr(this.strPathOne.length)
    // 				let strFileTwoFullPath = this.strPathTwo + strFileRelativePath
    // 				fs.stat(fullPath, (err, stats1) => {
    // 					if (stats1.isDirectory()) {
    // 						// console.log('Folder: ' + fullPath)
    // 						this.setFileChoices(fullPath)
    // 					} else {
    // 						if (fs.existsSync(strFileTwoFullPath)) {
    // 							fs.stat(strFileTwoFullPath, (err, stats2) => {
    // 								if (stats1.size != stats2.size) {
    // 									this.addFileChoice('Sizes Different: ' + fullPath)
    // 								} else if (stats1.mtime != stats2.mtime) {
    // 									this.addFileChoice('Dates Different: ' + fullPath)
    // 								}
    // 							})
    // 						} else {
    // 							this.addFileChoice('Added File: ' + fullPath)
    // 						}
    // 					}
    // 				})
    // 			} catch (err) { console.log(err) }
    // 		}
    // 	})
    // }

    // addFileChoice(Choice: string) {
    // 	this.Choices.push(Choice)
    // 	this.Choices[0] = '-----' + this.Choices.length.toLocaleString() + ' files'

    // }

    // fileLocated(fullPath, relativePath, statObject) {
    // 	if (statObject.directory && relativePath[0] === '.') {
    // 		return false  // do not delve deeper into hidden paths
    // 	}
    // 	if (statObject.directory && relativePath[0] === '.') {
    // 		return false  // do not delve deeper into hidden paths
    // 	}

    // 	let strFileRelativePath = fullPath.substr(this.strPathOne.length)
    // 	let strFileTwoFullPath = this.strPathTwo + strFileRelativePath
    // 	let booDiffFound: boolean = false
    // 	if (fs.existsSync(strFileTwoFullPath)) {
    // 		fs.stat(strFileTwoFullPath, (err, stats2) => {
    // 			if (statObject.size != stats2['size']) {
    // 				this.Choices.push('Size Different: ' + fullPath)
    // 				booDiffFound = true
    // 			} else if (statObject.mtime != stats2['mtime']) {
    // 				this.Choices.push('Dates Different: ' + fullPath)
    // 				booDiffFound = true
    // 			}
    // 		})

    // 	} else {
    // 		this.Choices.push('Added File: ' + fullPath)
    // 		booDiffFound = true
    // 	}

    // 	if (booDiffFound) {
    // 		this.Choices[0] = '-----' + this.Choices.length.toLocaleString() + ' files'

    // 		let oTimerEnd: moment.Moment = moment()
    // 		let oTimerDuration: moment.Duration = moment.duration(
    // 			oTimerEnd.diff(this.oTimerStart, 'milliseconds'),
    // 		)
    // 		let strFileCount: string = this.Choices.length.toLocaleString()
    // 		let strStatusLine: string = ''
    // 		if (oTimerDuration.seconds() == 1) {
    // 			strStatusLine = strFileCount + ' files in 1 second.'
    // 		} else if (oTimerDuration.seconds() < 60) {
    // 			strStatusLine = strFileCount + ' files in ' + oTimerDuration.seconds() + ' seconds.'
    // 		} else if (oTimerDuration.minutes() == 1) {
    // 			strStatusLine = strFileCount + ' files in 1 minute.'
    // 		} else {
    // 			strStatusLine = strFileCount + oTimerDuration.minutes() + ' minutes.'
    // 		} strStatusLine = 'Loading... - ' + strStatusLine
    // 		readline.moveCursor(process.stdout, 0, -2)
    // 		readline.cursorTo(process.stdout, 0)
    // 		readline.clearLine(process.stdout, 0)
    // 		process.stdout.write(strStatusLine)
    // 		readline.moveCursor(process.stdout, 0, 2)
    // 		readline.cursorTo(process.stdout, 0)
    // 	}
    // }
}

new DiffPatch()

// making aur files
// npm2PKGBUILD interactive-diff-patch > PKGBUILD
// makepkg --printsrcinfo > .SRCINFO
// add find "${pkgdir}"/usr -type d -exec chmod 755 {} +
// to last line of package section in PKGBUILD

// dpkg-buildpackage
// ??mk-build-deps -i debian/control
// sudo dpkg-sig --sign builder mypackage.deb
