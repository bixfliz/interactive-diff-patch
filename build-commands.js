const execSync = require('child_process').execSync
const deleteFile = require('fs').unlinkSync
const type = require('os').type
const find = require('find')
const packageDetails = require('./package.json')
let isLinux = false
let isMac = false
let isWin = false
let strArgString = '--||--' + process.argv.join('--||--') + '--||--'

if (type() === 'Linux') isLinux = true
if (type() === 'Darwin') isMac = true
if (type() === 'Windows_NT') isWin = true

function build() {
    console.log('building')
    if (isLinux || isMac) execSync('rm -rf dist', { stdio: 'inherit' })
    if (isWin) execSync('del /q /s dist', { stdio: 'inherit' })
    if (isLinux || isMac) execSync('node_modules/.bin/tsc', { stdio: 'inherit' })
    if (isWin) execSync('node_modules\\.bin\\tsc.cmd', { stdio: 'inherit' })
}

function pack() {
    build()
    console.log('packing')
    execSync('npm pack', { stdio: 'inherit' })
}

function run() {
    build()
    console.log('running')
    execSync('node dist/index.js', { stdio: 'inherit' })
}

function installFromLocal() {
    var strSudo = ''
    if (isLinux || isMac) strSudo = ' sudo '
    console.log('uninstalling')
    execSync(strSudo + 'npm uninstall -g ' + packageDetails.name, { stdio: 'inherit' })
    console.log('reinstalling')
    execSync(strSudo + ' npm i -g ' + packageDetails.name + '-' + packageDetails.version + '.tgz', { stdio: 'inherit' })

    console.log('info')
    execSync(strSudo + ' npm ll -g ' + packageDetails.name, { stdio: 'inherit' })
}

function removeTarBalls() {
    find.eachfile(/' + packageDetails.name + '-.*tgz$/, __dirname, function(files) {
        if (files && files.length) {
            console.log('Removing tgz file: ' + files)
            deleteFile(files)
        }
    })
}

try {
    if (strArgString.includes('--||--run--||--')) {
        run()
    } else if (strArgString.includes('--||--build--||--')) {
        build()
    } else if (strArgString.includes('--||--format--||--')) {
        execSync('prettier --write "src/**/*.ts"', { stdio: 'inherit' })
    } else if (strArgString.includes('--||--build-and-reinstall-local--||--')) {
        pack()
        installFromLocal()
        removeTarBalls()
    } else {
        console.log('Command missing')
    }
} catch (e) {
    console.log(e)
}
