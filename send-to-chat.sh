cd ~/Documents/code/interactive-diff-patch/
rm idiff.zip
zip idiff.zip . -x "node_modules/*" -x "dist/*" -r
matrix-commander -m "idiff code:" --room "\\!rrGzEdLyieKBzXzphm:matrix.org"
matrix-commander -f idiff.zip --room "\\!rrGzEdLyieKBzXzphm:matrix.org"
